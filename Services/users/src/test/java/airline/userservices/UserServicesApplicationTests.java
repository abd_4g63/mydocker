package airline.userservices;

import airline.userservices.model.Passenger;
import airline.userservices.model.Role;
import airline.userservices.model.User;
import airline.userservices.repository.PassengerRepository;
import airline.userservices.repository.UserRepository;
import airline.userservices.services.PassengerService;
import airline.userservices.services.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;

@SpringBootTest
//@RunWith(SpringRunner.class)
//@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
@Rollback(false)
class UserServicesApplicationTests {

    @Autowired
    UserService userService ;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PassengerService passengerService ;

    @Autowired
    PassengerRepository passengerRepository;

    @Test
    public void AddUser(){
        User user = new User();

        user.setUserName("test33");
        user.setPassword("1222");
        user.setEmail("test@gmail.com");
        user.setRole(Role.Admin);

        userService.saveuser(user);
    }

    @Test
    void UpdateUser()
    {
        User user=new User() ;
        for(User user1:userRepository.findAll())
            if (user1.getId()==1)
                user=user1;
        user.setUserName("fff");
        user.setPassword("333");
        user.setRole(Role.Customer);

        userService.saveuser(user);
    }
//
//
    @Test
    void DeleteUser()
    {
        User user=new User() ;
        user.setId(1L);
        userService.deleteUser(user);
    }
//
    @Test
    void FindAllUser()
    {
        List<User> user=userService.findAllUser();

    }
///////////////////////////////////////////////////////////////////////
    @Test
    public void AddPassenger(){
        Passenger passenger = new Passenger();

        passenger.setPassengerName("test67");
        passenger.setPhoneNumber(7667);
        passenger.setAge("999");
        passenger.setGender("f");

        passengerService.savepassenger(passenger);
    }

    @Test
    void UpdatePassenger()
    {
        Passenger passenger = new Passenger();
        for(Passenger passenger1:passengerRepository.findAll())
            if (passenger1.getId()==1)
                passenger=passenger1;
        passenger.setPassengerName("fff");
        passenger.setAge("333");
        passenger.setGender("ff");

        passengerService.savepassenger(passenger);
    }
    @Test
    void DeletePassenger()
    {
        Passenger passenger = new Passenger();
        passenger.setId(1L);
        passengerService.deletePassenger(passenger);
    }

    @Test
    void FindAllPassenger()
    {
        List<Passenger> passenger=passengerService.findAllPassenger();

    }

}
