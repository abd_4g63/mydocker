package airline.userservices.repository;

import airline.userservices.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<User,Long> {

    @Query("SELECT user FROM User user WHERE user.userName = ?1")
    User findUserByUserName(String userName);

}
