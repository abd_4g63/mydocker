package airline.userservices.repository;

import airline.userservices.model.Passenger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PassengerRepository extends JpaRepository<Passenger,Long> {

    @Query("SELECT passenger FROM Passenger passenger WHERE passenger.passengerName = ?1")
    Passenger findPassengerByName(String passengerName);
}
