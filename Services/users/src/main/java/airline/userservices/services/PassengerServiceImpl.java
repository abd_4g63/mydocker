package airline.userservices.services;

import airline.userservices.model.Passenger;
import airline.userservices.repository.PassengerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PassengerServiceImpl implements PassengerService{

    @Autowired
    PassengerRepository passengerRepository;

    @Override
    public Passenger savepassenger(Passenger passenger){
        return passengerRepository.save(passenger);
    }

    @Override
    public void deletePassenger(Passenger passenger){
        passengerRepository.delete(passenger);
    }

    @Override
    public Passenger updatePassenger(Passenger passenger){
        return passengerRepository.save(passenger);
    }

    @Override
    public Passenger findUser(int id) {
        for (Passenger u:passengerRepository.findAll()) {
            if (u.getId()==id)
                return u;
        }
        return null;
    }

    @Override
    public List<Passenger> findAllPassenger(){
        return passengerRepository.findAll();
    }

    @Override
    public Passenger findPassengerbyName(String passengerName){
        return passengerRepository.findPassengerByName(passengerName);
    }
}
