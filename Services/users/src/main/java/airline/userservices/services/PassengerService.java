package airline.userservices.services;

import airline.userservices.model.Passenger;

import java.util.List;

public interface PassengerService {
    Passenger savepassenger(Passenger passenger);

    void deletePassenger(Passenger passenger);

    Passenger updatePassenger(Passenger passenger);

    Passenger findUser(int id);

    List<Passenger> findAllPassenger();

    Passenger findPassengerbyName(String passengerName);
}
