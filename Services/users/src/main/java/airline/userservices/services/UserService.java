package airline.userservices.services;


import airline.userservices.model.User;

import java.util.List;

public interface UserService {
    User saveuser(User user);

    void deleteUser(User user);

    User updateUser(User user);

    User findUser(int id);

    List<User> findAllUser();

    User findUserByName(String userName);
}
