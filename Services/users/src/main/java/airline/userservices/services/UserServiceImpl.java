package airline.userservices.services;

import airline.userservices.model.User;
import airline.userservices.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository ;

    @Override
    public User saveuser(User user){
        return userRepository.save(user);
    }


    @Override
    public void deleteUser(User user){
        userRepository.delete(user);
    }

    @Override
    public User updateUser(User user){
        return userRepository.save(user);
    }

    @Override
    public User findUser(int id) {
        for (User u:userRepository.findAll()) {
            if (u.getId()==id)
                return u;
        }
        return null;
    }

    @Override
    public List<User> findAllUser(){
        return userRepository.findAll();
    }

    @Override
    public User findUserByName(String userName){
        return userRepository.findUserByUserName(userName);
    }

}
