package airline.userservices.controller;

import airline.userservices.model.Role;
import airline.userservices.model.User;
import airline.userservices.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping("/UserRegister")
    public ResponseEntity<?> userRegister(@RequestBody User user){
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encodedPassword = encoder.encode(user.getPassword());

        user.setPassword(encodedPassword);
        user.setRole(Role.Customer);
        return new ResponseEntity<>(userService.saveuser(user), HttpStatus.CREATED);
    }

    @PostMapping("/AdminRegister")
    public ResponseEntity<?> AdminRegister(@RequestBody User user){
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encodedPassword = encoder.encode(user.getPassword());

        user.setPassword(encodedPassword);
        user.setRole(Role.Admin);
        return new ResponseEntity<>(userService.saveuser(user), HttpStatus.CREATED);
    }

    @GetMapping("/findUserByName")
    public User findUserByName(@RequestBody String userName){
        return userService.findUserByName(userName);
    }

    @GetMapping("/deleteUser")
    public void deleteUserById(@RequestBody User user){
        userService.deleteUser(user);
    }


    @GetMapping("/findAllUser")
    public ResponseEntity<?> findAllUser(){
        return ResponseEntity.ok(userService.findAllUser());
    }

    @PostMapping("/updateUser")
    public ResponseEntity<?> updateUser(@RequestBody Long id){
        User user = new User();
        user.setId(id);
        user.setUserName(user.getUserName());
        user.setPassword(user.getPassword());
        user.setEmail(user.getEmail());
        user.setRole(user.getRole());

        return ResponseEntity.ok(userService.saveuser(user));
    }

}
