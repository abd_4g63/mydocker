package airline.userservices.controller;

import airline.userservices.model.Passenger;
import airline.userservices.services.PassengerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class PassengerController {

    @Autowired
    PassengerService passengerService;

    @GetMapping("/findPassengerByName")
    public Passenger findPassengerByName(@RequestBody String passengerName){
        return passengerService.findPassengerbyName(passengerName);
    }

    @GetMapping("/deletePassenger")
    public void deletePassengerById(@RequestBody Passenger passenger){
        passengerService.deletePassenger(passenger);
    }


    @GetMapping("/findAllPassenger")
    public ResponseEntity<?> findAllPassenger(){
        return ResponseEntity.ok(passengerService.findAllPassenger());
    }

    @PostMapping("/updatePassenger")
    public ResponseEntity<?> updatePassenger(@RequestBody Long id){
        Passenger passenger = new Passenger();
        passenger.setId(id);
        passenger.setPassengerName(passenger.getPassengerName());
        passenger.setAge(passenger.getAge());
        passenger.setGender(passenger.getGender());
        passenger.setPhoneNumber(passenger.getPhoneNumber());

        return ResponseEntity.ok(passengerService.savepassenger(passenger));
    }
}
