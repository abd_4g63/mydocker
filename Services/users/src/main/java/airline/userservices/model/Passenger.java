package airline.userservices.model;

import jakarta.persistence.*;

@Entity
@Table(name = "Passenger")
public class Passenger {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id ;

    @Column(name = "passengerName")
    private String passengerName ;

    @Column(name="age")
    private String age ;

    @Column(name = "gender")
    private String gender ;

//    @Column(name="seatType")
//    private String seatType ;

    @Column(name = "phoneNumber")
    private double phoneNumber ;

    public Passenger(Long id, String passengerName, String age, String gender, String seatType, double phoneNumber) {
        this.id = id;
        this.passengerName = passengerName;
        this.age = age;
        this.gender = gender;
//        this.seatType = seatType;
        this.phoneNumber = phoneNumber;
    }

    public Passenger() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPassengerName() {
        return passengerName;
    }

    public void setPassengerName(String passengerName) {
        this.passengerName = passengerName;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

//    public String getSeatType() {
//        return seatType;
//    }
//
//    public void setSeatType(String seatType) {
//        this.seatType = seatType;
//    }

    public double getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(double phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


}
