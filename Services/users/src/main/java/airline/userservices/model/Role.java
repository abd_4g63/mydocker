package airline.userservices.model;

public enum Role {
    Customer,
    Admin
}
