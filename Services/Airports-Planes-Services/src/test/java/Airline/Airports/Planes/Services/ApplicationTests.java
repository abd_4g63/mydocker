package Airline.Airports.Planes.Services;

import Airline.Airports.Planes.Services.Repository.AirportsRepository;
import Airline.Airports.Planes.Services.Repository.PlanesRepository;
import Airline.Airports.Planes.Services.model.Airports;
import Airline.Airports.Planes.Services.model.Planes;
import Airline.Airports.Planes.Services.services.AirportServices;
import Airline.Airports.Planes.Services.services.PlaneServices;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

@SpringBootTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
@Rollback(false)
class ApplicationTests {

	@Autowired
	AirportServices airportServices;

	@Autowired
	PlaneServices planeServices;

	@Autowired
	AirportsRepository airportsRepository;

	@Autowired
	PlanesRepository planesRepository;

	@Test
	public void AddPlane(){
		Planes plane = new Planes();
		plane.setPlanesId(4L);
		plane.setMaxWeight(20000);
		plane.setPlanesNum(44);
		plane.setPilot("ee");
		plane.setModel("we");
		plane.setSeatNum(55);

		planeServices.savePlane(plane);
	}

	@Test
	public void updatePlane(){
		Planes plane = new Planes();
		for(Planes plane1:planesRepository.findAll()){
			if(plane1.getPlanesId()==1)
				plane=plane1;

			plane.setSeatNum(66);
			planeServices.savePlane(plane);
		}

	}

	@Test
	public void deletePlane(){
		Planes plane = new Planes();
		plane.setPlanesId(2L);
		planeServices.deletePlane(plane);
	}

	@Test
	public void findAllPlanes(){
		planeServices.findAllPlanes();
	}

	////////////////////////////////////////////////////////////////////////////////////////

	@Test
	public void AddAirport(){
		Airports airport = new Airports();

		airport.setAirportsId(5L);
		airport.setAirportsName("wq");
		airport.setAirportsNum(32);
		airport.setCountry("tuu");
		airport.setCity("rtrt");

		airportServices.saveAirport(airport);
	}

	@Test
	public void updateAirport(){
		Airports airport = new Airports();
		for(Airports airport1:airportsRepository.findAll()){
			if(airport1.getAirportsId()==1)
				airport=airport1;

			airport.setAirportsNum(4000);
			airportServices.updateAirport(airport);
		}

	}

	@Test
	public void deleteAirport(){
		Airports airport = new Airports();
		airport.setAirportsId(2L);
		airportServices.deleteAirport(airport);
	}

	@Test
	public void findAllAirports(){
		airportServices.findAllAirports();
	}
}
