package Airline.Airports.Planes.Services.services;

import Airline.Airports.Planes.Services.Repository.AirportsRepository;
import Airline.Airports.Planes.Services.model.Airports;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AirportServicesImpl implements AirportServices {

    @Autowired
    AirportsRepository airportsRepository ;

    @Override
    public Airports saveAirport(Airports airports){
        return airportsRepository.save(airports);
    }

    @Override
    public List<Airports> findAllAirports(){
        return airportsRepository.findAll();
    }

    @Override
    public void deleteAirport(Airports airports){
        airportsRepository.delete(airports);
    }

    @Override
    public Airports findAirportByName(String AirportName){
        return airportsRepository.findAirportByName(AirportName);
    }

    @Override
    public Airports updateAirport(Airports airports){
        return airportsRepository.save(airports);
    }
}
