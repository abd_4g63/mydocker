package Airline.Airports.Planes.Services.Repository;

import Airline.Airports.Planes.Services.model.Airports;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface AirportsRepository extends JpaRepository<Airports,Long> {

    @Query("SELECT airport FROM Airports airport WHERE airport.AirportName = ?1")
    Airports findAirportByName(String AirportName);

    @Query("SELECT airport FROM Airports airport WHERE airport.AirportsId = ?1")
    Airports findAirportById(Long id);
}
