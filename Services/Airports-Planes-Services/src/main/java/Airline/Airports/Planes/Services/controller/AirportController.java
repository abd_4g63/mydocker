package Airline.Airports.Planes.Services.controller;

import Airline.Airports.Planes.Services.Repository.AirportsRepository;
import Airline.Airports.Planes.Services.model.Airports;
import Airline.Airports.Planes.Services.services.AirportServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Optional;

@Controller
public class AirportController {

    @Autowired
    AirportServices airportServices ;

    @Autowired
    AirportsRepository repo;

    @PostMapping("/saveAirport")
    public ResponseEntity<?> saveAirport(@RequestBody Airports airport){
        return ResponseEntity.ok(airportServices.saveAirport(airport));
    }

    @PostMapping("/updateAirport/{id}")
    public ResponseEntity<?> updateAirport(@PathVariable long id ,@RequestBody Airports updated){
        Optional<Airports> existingAirport = Optional.ofNullable(repo.findAirportById(id));
        if (existingAirport.isPresent()){
            Airports updatedAirport = existingAirport.get();

            updatedAirport.setAirportsNum(updated.getAirportsNum());
            updatedAirport.setAirportsName(updated.getAirportsName());
            updatedAirport.setCity(updated.getCity());
            updatedAirport.setCountry(updated.getCountry());
            repo.save(updatedAirport);
        return ResponseEntity.ok(airportServices.saveAirport(updatedAirport));
        }
        else
            return ResponseEntity.notFound().build();

    }

    @GetMapping("/deleteAirport")
    public void deleteAirport(Airports airport){
        airportServices.deleteAirport(airport);
    }

    @GetMapping("/findAllAirport")
    public ResponseEntity<?> findAllAirport(){
        return ResponseEntity.ok(airportServices.findAllAirports());
    }

    @GetMapping("/findAirportByName")
    public ResponseEntity<?> findAirportByName(@RequestBody String airportName){
        return ResponseEntity.ok(airportServices.findAirportByName(airportName));
    }
}
