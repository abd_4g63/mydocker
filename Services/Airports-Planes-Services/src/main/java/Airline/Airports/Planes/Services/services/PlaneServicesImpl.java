package Airline.Airports.Planes.Services.services;

import Airline.Airports.Planes.Services.Repository.PlanesRepository;
import Airline.Airports.Planes.Services.model.Planes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlaneServicesImpl implements PlaneServices {

    @Autowired
    PlanesRepository planesRepository;

    @Override
    public Planes savePlane(Planes planes){
        return planesRepository.save(planes);
    }

    @Override
    public Planes updatePlane(Planes planes){
        return planesRepository.save(planes);
    }

    @Override
    public void deletePlane(Planes planes){
        planesRepository.delete(planes);
    }

    @Override
    public List<Planes> findAllPlanes(){
        return planesRepository.findAll();
    }

    @Override
    public Planes findPlaneByModel(String planeModel){
        return planesRepository.findPlanesByModel(planeModel);
    }

}
