package Airline.Airports.Planes.Services.controller;

import Airline.Airports.Planes.Services.model.Planes;
import Airline.Airports.Planes.Services.services.PlaneServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Controller
public class PlaneController {

    @Autowired
    PlaneServices planeServices ;

    @PostMapping("/savePlane")
    public ResponseEntity<?> savePlane(@RequestBody Planes planes){
        return  ResponseEntity.ok(planeServices.savePlane(planes));
    }

    @GetMapping("/updatePlane")
    public ResponseEntity<?> updatePlane(@RequestBody Long id){
        Planes plane = new Planes();
        plane.setPlanesId(id);
        plane.setPlanesNum(plane.getPlanesNum());
        plane.setModel(plane.getModel());
        plane.setPilot(plane.getPilot());
        plane.setSeatNum(plane.getSeatNum());
        plane.setMaxWeight(plane.getMaxWeight());

        return ResponseEntity.ok(planeServices.updatePlane(plane));
    }

    @GetMapping("/deletePlane")
    public void deletePlane(@RequestBody Planes plane){
        planeServices.deletePlane(plane);
    }

    @GetMapping("/findAllPlanes")
    public ResponseEntity<?> findAllPlanes(){
        return ResponseEntity.ok(planeServices.findAllPlanes()) ;
    }

    @GetMapping("/findPlaneByModel")
    public ResponseEntity<?> findPlaneByModel(@RequestBody String planeModel){
        return ResponseEntity.ok(planeServices.findPlaneByModel(planeModel));
    }
}
