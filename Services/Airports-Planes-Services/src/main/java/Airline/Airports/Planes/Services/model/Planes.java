package Airline.Airports.Planes.Services.model;


import jakarta.persistence.*;

@Entity
@Table(name = "Planes")
public class Planes {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long planesId ;

    @Column(name = "planesNum")
    private int planesNum ;

    @Column(name = "pilot")
    private String pilot ;

    @Column(name = "seatNum")
    private int seatNum ;

    @Column(name = "model")
    private String model ;

    @Column(name = "maxWeight")
    private int maxWeight ;

    public Planes(Long planesId, int planesNum, String pilot, int seatNum, String model, int maxWeight) {
        this.planesId = planesId;
        this.planesNum = planesNum;
        this.pilot = pilot;
        this.seatNum = seatNum;
        this.model = model;
        this.maxWeight = maxWeight;
    }

    public Planes() {

    }

    public Long getPlanesId() {
        return planesId;
    }

    public void setPlanesId(Long planesId) {
        this.planesId = planesId;
    }

    public int getPlanesNum() {
        return planesNum;
    }

    public void setPlanesNum(int planesNum) {
        this.planesNum = planesNum;
    }

    public String getPilot() {
        return pilot;
    }

    public void setPilot(String pilot) {
        this.pilot = pilot;
    }

    public int getSeatNum() {
        return seatNum;
    }

    public void setSeatNum(int seatNum) {
        this.seatNum = seatNum;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getMaxWeight() {
        return maxWeight;
    }

    public void setMaxWeight(int maxWeight) {
        this.maxWeight = maxWeight;
    }
}
