package Airline.Airports.Planes.Services.Repository;

import Airline.Airports.Planes.Services.model.Airports;
import Airline.Airports.Planes.Services.model.Planes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PlanesRepository extends JpaRepository<Planes,Long> {

    @Query("SELECT planes FROM Planes planes WHERE planes.model = ?1")
    Planes findPlanesByModel(String PlaneModel);
}
