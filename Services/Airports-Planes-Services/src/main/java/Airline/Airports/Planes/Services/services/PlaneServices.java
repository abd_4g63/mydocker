package Airline.Airports.Planes.Services.services;

import Airline.Airports.Planes.Services.model.Planes;

import java.util.List;

public interface PlaneServices {
    Planes savePlane(Planes planes);

    Planes updatePlane(Planes planes);

    void deletePlane(Planes planes);

    List<Planes> findAllPlanes();

    Planes findPlaneByModel(String planeModel);
}
