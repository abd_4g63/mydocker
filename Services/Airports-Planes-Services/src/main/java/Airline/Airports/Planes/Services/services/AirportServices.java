package Airline.Airports.Planes.Services.services;

import Airline.Airports.Planes.Services.model.Airports;

import java.util.List;

public interface AirportServices {
    Airports saveAirport(Airports airports);

    List<Airports> findAllAirports();

    void deleteAirport(Airports airports);

    Airports findAirportByName(String AirportName);

    Airports updateAirport(Airports airports);
}
