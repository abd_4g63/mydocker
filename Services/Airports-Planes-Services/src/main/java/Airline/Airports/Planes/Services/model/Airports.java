package Airline.Airports.Planes.Services.model;

import jakarta.persistence.*;


@Entity
@Table(name = "Airports")
public class Airports {

    @Id
    @GeneratedValue(strategy =GenerationType.IDENTITY)
    private Long AirportsId ;

    @Column(name = "AirportsNum")
    private int AirportsNum ;

    @Column(name = "Country")
    private String Country ;

    @Column(name = "City")
    private String City ;

    @Column(name = "AirportsName")
    private String AirportName ;

    public Airports(Long airportsId, int airportsNum, String country, String city, String airportsName) {
        AirportsId = airportsId;
        AirportsNum = airportsNum;
        Country = country;
        City = city;
        AirportName = airportsName;
    }

    public Airports() {

    }

    public Long getAirportsId() {
        return AirportsId;
    }

    public void setAirportsId(Long airportsId) {
        AirportsId = airportsId;
    }

    public int getAirportsNum() {
        return AirportsNum;
    }

    public void setAirportsNum(int airportsNum) {
        AirportsNum = airportsNum;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getAirportsName() {
        return AirportName;
    }

    public void setAirportsName(String airportsName) {
        AirportName = airportsName;
    }
}
