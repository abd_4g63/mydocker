package Airline.BookingServices;

import Airline.BookingServices.model.Booking;
import Airline.BookingServices.repository.BookingRepository;
import Airline.BookingServices.services.BookingServices;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;

@SpringBootTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
@Rollback(false)
class BookingServicesApplicationTests {

	@Autowired
	BookingServices bookingServices ;

	@Autowired
	BookingRepository bookingRepository ;

	@Test
	public void AddBooking(){
		Booking booking = new Booking();

		booking.setBookingId(144L);
		booking.setBookingConf(false);
		booking.setPayConf(false);
//		booking.setBookingDate();
		booking.setTripId(225L);
		booking.setUserId(445L);
		booking.setPassengerNum(334);

		bookingServices.saveBooking(booking);
	}

	@Test
	public void  confpay(){

		Booking booking = new Booking();
		for (Booking booking1:bookingRepository.findAll()){
			if(booking1.getBookingId()==3){
				booking=booking1;

				booking.setPayConf(true);
				bookingServices.saveBooking(booking);
			}
		}
	}

	@Test
	public void confBooking() {
		Booking booking = new Booking();
		for (Booking booking1 : bookingRepository.findAll()) {
			if (booking1.getBookingId() == 3 && booking1.isPayConf() == true) {
				booking = booking1;
				booking.setBookingConf(true);
				bookingServices.saveBooking(booking);
			}
		}
	}

	@Test
	public void updateBooking(){
		Booking booking = new Booking();
		for (Booking booking1:bookingRepository.findAll()) {
			if (booking1.getBookingId() == 3) {
				booking = booking1;

				booking.setTripId(55L);
				bookingServices.saveBooking(booking);
			}
		}
	}

	@Test
	public void deleteBooking(){
		Booking booking=new Booking() ;
		booking.setBookingId(1L);
		bookingServices.deleteBooking(booking);
	}

	@Test
	void FindAllBooking()
	{
		List<Booking> bookings=bookingServices.findAllBooking();

	}

}
