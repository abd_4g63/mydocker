package Airline.BookingServices.controller;

import Airline.BookingServices.model.Booking;
import Airline.BookingServices.repository.BookingRepository;
import Airline.BookingServices.services.BookingServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class BookingController {

    @Autowired
    BookingServices bookingServices;

    @Autowired
    BookingRepository bookingRepository;

    @PostMapping("/saveBooking")
    public ResponseEntity<?> saveBooking(Booking booking) {
        return new ResponseEntity<>(bookingServices.saveBooking(booking), HttpStatus.CREATED);
    }

    @GetMapping("/deleteBooking")
    public void deleteBooking(Booking booking) {
        bookingServices.deleteBooking(booking);
    }

    @GetMapping("/findAllBooking")
    public ResponseEntity<?> findAllBooking() {
        return ResponseEntity.ok(bookingServices.findAllBooking());
    }

    @GetMapping("/findBookingByUserId")
    public ResponseEntity<?> findBookingByUserId(@RequestBody Long userId) {
        return ResponseEntity.ok(bookingServices.findBookingByUserId(userId));
    }

    @GetMapping("/updateBooking")
    public ResponseEntity<?> updateBooking(@RequestBody Long id) {
        Booking booking = new Booking();
        booking.setBookingId(id);
        booking.setBookingConf(booking.isBookingConf());
        booking.setPayConf(booking.isPayConf());
        booking.setBookingDate(booking.getBookingDate());
        booking.setTripId(booking.getTripId());
        booking.setUserId(booking.getUserId());
        booking.setPassengerNum(booking.getPassengerNum());
        return ResponseEntity.ok(bookingServices.saveBooking(booking));
    }

//    @GetMapping("/confBooking")
//    public ResponseEntity<?> confBooking (@RequestBody Booking booking){
//        if(booking.isPayConf()==true) {
//            Booking booking1 = new Booking();
//            booking1.setBookingId(booking.getBookingId());
//            booking1.setBookingConf(booking.isBookingConf());
//        }
//        return ResponseEntity.ok(bookingServices.saveBooking(booking));
//    }
//
//    @GetMapping("/confPay")
//    public ResponseEntity<?> confPay(@RequestBody Long id ){
//        Booking booking = new Booking();
//        booking.setBookingId(id);
//        booking.setPayConf(booking.isPayConf());
//        return ResponseEntity.ok(bookingServices.saveBooking(booking));
//    }

//    public ResponseEntity<?> cansellBooking(@RequestBody Long id ) {
//        Booking booking = new Booking();
//        booking.setBookingId(id);
//        if (booking.isBookingConf() == true) {
//            booking.setBookingConf(false);
//            return ResponseEntity.ok(bookingServices.saveBooking(booking));
//        } else
//            return null;
//    }

    @GetMapping("/confPay")
    public ResponseEntity confPay(@RequestBody Long id) {
        Booking booking = new Booking();
        for (Booking booking1 : bookingRepository.findAll()) {
            if (booking1.getBookingId() == id) {
                booking = booking1;

                booking.setPayConf(true);
            }
        }
        return ResponseEntity.ok(bookingServices.saveBooking(booking));
    }

    @GetMapping("/cansellPay")
    public ResponseEntity cansellPay(@RequestBody Long id) {
        Booking booking = new Booking();
        for (Booking booking1 : bookingRepository.findAll()) {
            if (booking1.getBookingId() == id) {
                booking = booking1;

                booking.setPayConf(false);
            }
        }
        return ResponseEntity.ok(bookingServices.saveBooking(booking));
    }

    @GetMapping("/confBooking")
    public ResponseEntity confBooking(@RequestBody Long id) {
        Booking booking = new Booking();
        for (Booking booking1 : bookingRepository.findAll()) {
            if (booking1.getBookingId() == id) {
                booking = booking1;

                booking.setBookingConf(true);
            }
        }
        return ResponseEntity.ok(bookingServices.saveBooking(booking));
    }

    @GetMapping("/cansellBooking")
    public ResponseEntity cansellBooking(@RequestBody Long id) {
        Booking booking = new Booking();
        for (Booking booking1 : bookingRepository.findAll()) {
            if (booking1.getBookingId() == id) {
                booking = booking1;

                booking.setBookingConf(false);
            }
        }
        return ResponseEntity.ok(bookingServices.saveBooking(booking));
    }

}
