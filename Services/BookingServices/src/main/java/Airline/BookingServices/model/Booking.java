package Airline.BookingServices.model;

import jakarta.persistence.*;
import org.springframework.boot.autoconfigure.web.WebProperties;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Entity
@Table(name="Booking")
public class Booking {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long BookingId;

    @Column(name="TripId")
    private Long TripId;

    @Column(name="UserId")
    private Long UserId;

    @Column(name="passengernum")
    private int passengerNum ;

    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Column(name="BookingDate")
    private Date BookingDate ;

    @Column(name="BookingConf")
    private boolean BookingConf=false;

    @Column(name="PayConf")
    private boolean PayConf=false ;

    public Booking(Long bookingId, Long tripId, Long userId, int passengerNum, Date bookingDate, boolean bookingConf, boolean payConf) {
        BookingId = bookingId;
        TripId = tripId;
        UserId = userId;
        this.passengerNum = passengerNum;
        BookingDate = bookingDate;
        BookingConf = bookingConf;
        PayConf = payConf;
    }

    public Booking() {

    }

    public Long getBookingId() {
        return BookingId;
    }

    public void setBookingId(Long bookingId) {
        BookingId = bookingId;
    }

    public Long getTripId() {
        return TripId;
    }

    public void setTripId(Long tripId) {
        TripId = tripId;
    }

    public Long getUserId() {
        return UserId;
    }

    public void setUserId(Long userId) {
        UserId = userId;
    }

    public int getPassengerNum() {
        return passengerNum;
    }

    public void setPassengerNum(int passengerNum) {
        this.passengerNum = passengerNum;
    }

    public Date getBookingDate() {
        return BookingDate;
    }

    public void setBookingDate(Date bookingDate) {
        BookingDate = bookingDate;
    }

    public boolean isBookingConf() {
        return BookingConf;
    }

    public void setBookingConf(boolean bookingConf) {
        BookingConf = bookingConf;
    }

    public boolean isPayConf() {
        return PayConf;
    }

    public void setPayConf(boolean payConf) {
        PayConf = payConf;
    }


}
