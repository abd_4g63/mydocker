package Airline.BookingServices.services;

import Airline.BookingServices.model.Booking;
import Airline.BookingServices.repository.BookingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookingServicesImpl implements BookingServices{

    @Autowired
    BookingRepository bookingRepository ;

    @Override
    public Booking saveBooking(Booking booking){
        return bookingRepository.save(booking);
    }

    @Override
    public void deleteBooking(Booking booking){
        bookingRepository.delete(booking);
    }

    @Override
    public Booking updateBooking(Booking booking){
        return bookingRepository.save(booking);
    }
    @Override
    public List<Booking> findAllBooking(){
        return bookingRepository.findAll();
    }

    @Override
    public Booking findBookingByUserId(Long userId){
        return bookingRepository.findBookingByUserId(userId);
    }



}
