package Airline.BookingServices.services;

import Airline.BookingServices.model.Booking;

import java.util.List;

public interface BookingServices {
    Booking saveBooking(Booking booking);

    void deleteBooking(Booking booking);

    Booking updateBooking(Booking booking);

    List<Booking> findAllBooking();

    Booking findBookingByUserId(Long userId);
}
