package Airline.BookingServices.repository;

import Airline.BookingServices.model.Booking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BookingRepository extends JpaRepository<Booking,Long> {

    @Query("SELECT booking FROM Booking booking WHERE booking.UserId = ?1")
    Booking findBookingByUserId(Long userId);
}
