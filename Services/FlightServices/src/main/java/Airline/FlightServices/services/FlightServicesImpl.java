package Airline.FlightServices.services;

import Airline.FlightServices.model.Flight;
import Airline.FlightServices.repository.FlightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FlightServicesImpl implements FlightServices{

    @Autowired
    FlightRepository flightRepository ;

    @Override
    public Flight searchFlight(String source, String destination, String date, boolean availableSeats){
        return flightRepository.searchFlight(source,destination,date,availableSeats);
    }


    @Override
    public Flight saveFlight(Flight flight){
        return flightRepository.save(flight);
    }

    @Override
    public Flight searchFlightByName(String flightName){
        return flightRepository.searchFlightByName(flightName);
    }

    @Override
    public void deleteFlight(Flight flight){
        flightRepository.delete(flight);
    }

    @Override
    public Flight updateFlight(Flight flight){
        return flightRepository.save(flight);
    }
}
