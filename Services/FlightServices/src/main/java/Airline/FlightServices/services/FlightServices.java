package Airline.FlightServices.services;

import Airline.FlightServices.model.Flight;

import java.util.Date;

public interface FlightServices {
    Flight searchFlight(String source, String destination, String date, boolean availableSeats);

    Flight saveFlight(Flight flight);

    Flight searchFlightByName(String flightName);

    void deleteFlight(Flight flight);

    Flight updateFlight(Flight flight);
}
