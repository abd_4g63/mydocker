package Airline.FlightServices.controller;

import Airline.FlightServices.model.Flight;
import Airline.FlightServices.repository.FlightRepository;
import Airline.FlightServices.services.FlightServices;
import org.bouncycastle.math.ec.ECPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Date;

@Controller
public class FlightController  {

    @Autowired
    FlightServices flightServices;
    @Autowired
    private FlightRepository flightRepository;

    @PostMapping("/saveFlight")
    public ResponseEntity<?> saveFlight(@RequestBody Flight flight){
        return new ResponseEntity<>(flightServices.saveFlight(flight), HttpStatus.CREATED) ;
    }

    @GetMapping("/searchFlight")
    public ResponseEntity<?> searchFlight(@RequestBody String source , String destination , String date , boolean availableSeats){
        return ResponseEntity.ok(flightServices.searchFlight(source,destination,date,availableSeats));
    }

    @GetMapping("/findFlightByName")
    public ResponseEntity<?> findFlightByName(@RequestBody String flightName ){
        return ResponseEntity.ok(flightServices.searchFlightByName(flightName));
    }

    @GetMapping("/deleteFlight")
    public void deleteFlight(Flight flight){
         flightServices.deleteFlight(flight);
    }

    @GetMapping("/updateFlight")
    public Flight updateFlight(Long id){
        Flight flight = new Flight();

        flight.setFlightId(id);
        flight.setFlightname(flight.getFlightname());
        flight.setSource(flight.getSource());
        flight.setDestination(flight.getDestination());
        flight.setPrice(flight.getPrice());
        flight.setSeatNumber(flight.getSeatNumber());
        flight.setTypeSeat(flight.getTypeSeat());
        flight.setDate(flight.getDate());
        flight.setAvailableSeat(flight.isAvailableSeat());
        flight.setAvailableFlight(flight.isAvailableFlight());

        return flightServices.saveFlight(flight);
    }

    @GetMapping("/canselFlight")
    public ResponseEntity canselFlight(@RequestBody Long id) {
        Flight flight = new Flight();
        for (Flight flight1 : flightRepository.findAll()) {
            if (flight1.getFlightId() == id) {
                flight = flight1;

                flight.setAvailableFlight(false);
            }
        }
        return ResponseEntity.ok(flightServices.saveFlight(flight));
    }

    //maxSeats from Plane Services
    //bookSeats from bookingServices
    @GetMapping("/availableSeats")
    public void availableSeats(@RequestBody int maxSeats , int bookSeats , Long flightId){
        Flight flight = new Flight();
        flight.setFlightId(flightId);

        if (bookSeats > maxSeats){
             flight.setAvailableSeat(false) ;
        }
        else flight.setAvailableSeat(true);
    }
}
