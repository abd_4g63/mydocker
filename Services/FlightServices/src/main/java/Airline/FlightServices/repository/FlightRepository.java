package Airline.FlightServices.repository;

import Airline.FlightServices.model.Flight;
import org.bouncycastle.asn1.dvcs.Data;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;

public interface FlightRepository extends JpaRepository<Flight,Long> {

    @Query("SELECT flight FROM Flight flight WHERE flight.source = ?1 AND flight.destination=?1 AND flight.date=?1 AND flight.availableSeat=true ")
    Flight searchFlight(String source , String destination , String date , boolean availableSeats);


    @Query("SELECT flight FROM Flight flight WHERE flight.flightname = ?1")
    Flight searchFlightByName(String flightName);

}
