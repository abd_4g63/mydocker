package Airline.FlightServices.model;

import jakarta.persistence.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.awt.*;
import java.util.Date;

@Entity
@Table(name = "Flight")
public class Flight {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long flightId ;

    @Column(name = "flightname")
    private String flightname ;
    @Column(name = "source")
    private String source ;

    @Column(name = "destination")
    private String destination ;


    @Column(name = "date")
    private String date ;

    @Column(name = "seatNumber")
    private int seatNumber ;

    @Column(name = "typeSeat")
    private String typeSeat ;

    @Column(name = "availableFlight")
    private boolean availableFlight=false ;

    @Column(name = "availableSeat")
    private boolean availableSeat=false ;

    @Column(name = "price")
    private int price ;

    public Flight(Long flightId, String source, String destination, String date, int seatNumber, String typeSeat, boolean availableFlight, int price,boolean availableSeat ,String flightname) {
        this.flightId = flightId;
        this.flightname = flightname;
        this.source = source;
        this.destination = destination;
        this.date = date;
        this.seatNumber = seatNumber;
        this.typeSeat = typeSeat;
        this.availableFlight = availableFlight;
        this.availableSeat = availableSeat;
        this.price = price;
    }

    public Flight() {

    }

    public Long getFlightId() {
        return flightId;
    }

    public void setFlightId(Long flightId) {
        this.flightId = flightId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getFlightname() {
        return flightname;
    }

    public void setFlightname(String flightname) {
        this.flightname = flightname;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(int seatNumber) {
        this.seatNumber = seatNumber;
    }

    public String getTypeSeat() {
        return typeSeat;
    }

    public void setTypeSeat(String typeSeat) {
        this.typeSeat = typeSeat;
    }

    public boolean isAvailableFlight() {
        return availableFlight;
    }

    public void setAvailableFlight(boolean availableFlight) {
        this.availableFlight = availableFlight;
    }

    public boolean isAvailableSeat() {
        return availableSeat;
    }

    public void setAvailableSeat(boolean availableSeat) {
        this.availableSeat = availableSeat;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }


}
