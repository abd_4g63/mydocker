package Airline.FlightServices;

import Airline.FlightServices.model.Flight;
import Airline.FlightServices.repository.FlightRepository;
import Airline.FlightServices.services.FlightServices;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import java.awt.*;
import java.util.Date;

@SpringBootTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
@Rollback(false)
class FlightServicesApplicationTests {

    @Autowired
    FlightServices flightServices ;

    @Autowired
    FlightRepository flightRepository ;


    @Test
    public void AddFlight(){
        Flight flight = new Flight();


        flight.setFlightId(4L);
        flight.setAvailableFlight(true);
        flight.setAvailableSeat(true);
        flight.setFlightname("ww");
        flight.setPrice(4000);
        flight.setSource("yu");
        flight.setDate("1-2-2000");
        flight.setDestination("yyyy");

        flightServices.saveFlight(flight);
    }

    @Test
    public void searchFlight(){
        flightServices.searchFlight("yut","","",true);
    }

    @Test
    public void searchFlightByName(){
        flightRepository.searchFlightByName("wr");
    }

    @Test
    public void updateBooking(){
        Flight flight = new Flight();
        for (Flight flight1:flightRepository.findAll()) {
            if (flight1.getFlightId() == 1) {
                flight = flight1;

                flight.setFlightId(1L);
                flight.setPrice(6000);

                flightServices.saveFlight(flight);
            }
        }
    }

}
